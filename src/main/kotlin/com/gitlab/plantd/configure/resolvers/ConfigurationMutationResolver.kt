package com.gitlab.plantd.configure.resolvers

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import com.gitlab.plantd.configure.entity.Configuration
import com.gitlab.plantd.configure.repository.ConfigurationRepository
import org.springframework.stereotype.Component
import java.util.*

@Component
class ConfigurationMutationResolver (private val configurationRepository: ConfigurationRepository): GraphQLMutationResolver {
    fun newConfiguration(namespace: String): Configuration {
        val configuration = Configuration(namespace)
        configuration.id = UUID.randomUUID().toString()
        configurationRepository.save(configuration)
        return configuration
    }

    fun deleteConfiguration(id: String): Boolean {
        configurationRepository.deleteById(id)
        return true
    }

    fun updateConfiguration(id: String, namespace: String): Configuration {
        val configuration = configurationRepository.findById(id)
        configuration.ifPresent {
            it.namespace = namespace
            configurationRepository.save(it)
        }
        return configuration.get()
    }
}