package com.gitlab.plantd.configure.entity

data class Property (
        var key: String,
        var value: String
)
