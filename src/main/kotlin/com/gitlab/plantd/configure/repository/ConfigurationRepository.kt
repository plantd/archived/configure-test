package com.gitlab.plantd.configure.repository

import com.gitlab.plantd.configure.entity.Configuration
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface ConfigurationRepository : MongoRepository<Configuration, String>