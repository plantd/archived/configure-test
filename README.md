![Build Status](https://gitlab.com/plantd/configure/badges/master/build.svg)

---

# Plantd Configure Service

A service that provides a GraphQL API for configuration documents stored in
MongoDB.

## Running

The easiest way to run the service is using the Docker container that's
available in the GitLab registry.

```sh
docker pull registry.gitlab.com/plantd/configure:v1
docker run -it --rm -p 9000:9000 \
  -e PLANTD_MONGODB_DATABASE="plantd" \
  -e PLANTD_MONGODB_HOST="127.0.0.1" \
  -e PLANTD_MONGODB_PORT="27017" \
  registry.gitlab.com/plantd/configure:v1
```

## Developing

During development the service can be run using the included `gradle` script.

```sh
./gradlew bootRun
```

### Docker

To run locally in a Docker container use the following.

```sh
./gradlew bootJar
docker build -t plantd-configure .
docker run -it --rm -p 9000:9000 \
  -e PLANTD_MONGODB_DATABASE="plantd" \
  -e PLANTD_MONGODB_HOST="127.0.0.1" \
  -e PLANTD_MONGODB_PORT="27017" \
  plantd-configure
```

## Publishing

This service is published to the repository container registry using these
steps.

```sh
./gradlew bootJar
docker build -t registry.gitlab.com/plantd/configure:v1 .
docker push registry.gitlab.com/plantd/configure:v1
```
