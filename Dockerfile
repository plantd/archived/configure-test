FROM openjdk:8

COPY build/libs/configure-0.0.1-SNAPSHOT.jar /usr/bin/plantd-configure

CMD ["java", "-jar", "/usr/bin/plantd-configure"]
