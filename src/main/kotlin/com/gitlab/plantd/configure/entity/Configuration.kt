package com.gitlab.plantd.configure.entity

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "present")
data class Configuration (
        var namespace: String
) {
    @Id
    var id: String = ""
    var objects: List<Object> = ArrayList()
    var properties: List<Property> = ArrayList()
}