package com.gitlab.plantd.configure.entity

import org.springframework.data.annotation.Id

data class Object (
        var name: String,
        var type: String
) {
    @Id
    var id: String = ""
    var objects: List<Object> = ArrayList()
    var properties: List<Property> = ArrayList()
}