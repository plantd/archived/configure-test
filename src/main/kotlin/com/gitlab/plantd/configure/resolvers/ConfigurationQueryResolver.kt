package com.gitlab.plantd.configure.resolvers

import com.gitlab.plantd.configure.entity.Configuration
import com.gitlab.plantd.configure.repository.ConfigurationRepository
import com.coxautodev.graphql.tools.GraphQLQueryResolver
import org.springframework.data.mongodb.core.MongoOperations
import org.springframework.stereotype.Component

@Component
class ConfigurationQueryResolver(val configurationRepository: ConfigurationRepository,
                                 private val mongoOperations: MongoOperations) : GraphQLQueryResolver {
    fun configurations(): List<Configuration> {
        val list = configurationRepository.findAll()
        //for (item in list) {
        //    item.objects = getObjects(configurationId = item.id)
        //}
        return list
    }

    //private fun getObjects(configurationId: String): List<Object> {
    //    val query = Query()
    //    query.addCriteria(Criteria.where("configurationId").`is`(configurationId))
    //    return mongoOperations.find(query, Object::class.java)
    //}
}