package com.gitlab.plantd.configure

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ConfigureApplication

fun main(args: Array<String>) {
	runApplication<ConfigureApplication>(*args)
}
